# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Neeraj Rajesh
# <nrajesh@hawk.iit.edu>, Hector Hugo Hernandez Trivino
# <hhernandeztrivino@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HProfile
# 
# HProfile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
import configparser
import json
import subprocess

def bash_jason(cmd):
    return json.loads(subprocess.check_output(cmd, shell=True).decode('utf-8'))

if __name__ == '__main__':
    conf=configparser.ConfigParser()
    conf.read('probe_config.config')
    fio_loc=dict(conf.defaults().items())['fio_install']
    try :
        print(bash_jason(fio_loc+'/fio --output-format=json --name=global --rw=read --size=4K --name=job1 --directory=/home/neeraj --direct=1 --fsync=1 '))
    except subprocess.CalledProcessError as e:
        print("Run fio_setup.sh and try again")
