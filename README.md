# README 
This is a simple system profiler that can be used to evaluate 
the capabilities an individual node hardware by running
a set of benchmarks.

## Running the code

In order to install we can just write:

> make

This will install the fio library that is required to perform the profiling.

The benchmarking process is interactive. In order to run it you can type:

> python probing.py

This program will ask you for the type of benchmark that you want for evaluations. There are two outputs. 

**benchmarks.csv**: This is the result of running the benchmarks on the system

**mounted.csv**: this is a list of mounting points in the system and their hardware characteristics.