#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Neeraj Rajesh
# <nrajesh@hawk.iit.edu>, Hector Hugo Hernandez Trivino
# <hhernandeztrivino@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HProfile
# 
# HProfile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
loc="$(pwd)"
install_loc="$loc/install/"
zip_name="fio-master" # this is the name the archive extracts to.
unzip_loc="$loc/$zip_name/" ## need to figure this -d puts it in that folder

## get and make fio
echo "Downloading FIO"
wget -O $zip_name.zip https://github.com/axboe/fio/archive/master.zip
unzip -o $zip_name.zip
cd "$zip_name"
echo "Compiling and installing"
bash configure --prefix="$install_loc"
make
make install
cd -

## make config file
echo "Making config file"
echo "[FIO]" >> probe_config.config
echo "fio_install=$install_loc/bin/" >> probe_config.config
