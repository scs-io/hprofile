# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Neeraj Rajesh
# <nrajesh@hawk.iit.edu>, Hector Hugo Hernandez Trivino
# <hhernandeztrivino@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HProfile
# 
# HProfile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
import configparser
import json
import subprocess
from benchmark_maker import get_fio_command_list

def bash_jason(cmd):
    return json.loads(subprocess.check_output(cmd, shell=True).decode('utf-8'))

def run_one_fio(cmd):  
    try :
        print(bash_jason(cmd))
    except subprocess.CalledProcessError as e:
        print("Run fio_setup.sh and try again")

def main():
    conf=configparser.ConfigParser()
    conf.read('probe_config.config')
    fio_loc=conf['FIO']['fio_install']
    
    for i in get_fio_command_list():
        run_one_fio(fio_loc+'/'+i)
        
if __name__=='__main__':
    main()
    
    
