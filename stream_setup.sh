#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Neeraj Rajesh
# <nrajesh@hawk.iit.edu>, Hector Hugo Hernandez Trivino
# <hhernandeztrivino@hawk.iit.edu>, Xian-He Sun <sun@iit.edu>
#
# This file is part of HProfile
# 
# HProfile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

stream_name="stream_5-10_posix_memalign"
loc="$(pwd)"
install_loc="$loc/install/bin/"

echo "Downloading Stream"
curl  https://asc.llnl.gov/coral-2-benchmarks/downloads/stream_5-10_posix_memalign.c >  $stream_name.c

echo "Compiling Stream"
gcc -openmp -O3 -o $stream_name $stream_name.c

echo "Moving to install directory"
if [ ! -d "$loc/install" ]; then
  mkdir "$loc/install"
  mkdir "$install_loc"
fi
mv $stream_name $install_loc/$stream_name

echo "Making config"
echo "[STREAM]" >> probe_config.config
echo "stream_install=$install_loc" >> probe_config.config
